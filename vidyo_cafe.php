<?php 

require_once('conf.php');

// Vidyo soap connection
$admin_url = "https://vidyoportal.cern.ch/services/v1_1/VidyoPortalAdminService?wsdl";
$user_url = "https://vidyoportal.cern.ch/services/v1_1/VidyoPortalUserService?wsdl";
$credentials = array('login' => $API_LOGIN['user'], 'password' => $API_LOGIN['pwd'], 'trace' => 1);
$adminClient = new SoapClient($admin_url, $credentials);
$userClient = new SoapClient($user_url, $credentials);

// Get the participants of Cafe Room from Vidyo API
try {
        $room = 'vidyocafe';
	$getRooms = $adminClient->GetRooms(array('Filter' => array('query' => $room)));
	$room = $getRooms->room;
	$roomID = $room->roomID;
	$result = $adminClient->GetParticipants(array('conferenceID' => $roomID));
} catch (SoapFault $fault) {
	echo "Sorry, Admin API returned the following ERROR:\n".$fault->faultcode."-".$fault->faultstring."\n";
}

// Detect if Vidyo Cafe is connected to Cafe Room
$bool = FALSE;
$displayName = 'Vidyo Cafe';
if ($result->total > 0) {
	$cafe_participants = $result->Entity;
	if (is_array($cafe_participants)) {
		foreach ($cafe_participants as $participant) {
			$name = $participant->displayName;
			if (strpos($name, $displayName) !== false) {
				$bool = TRUE;
				break;
			}
		}
	} else {
		$name = $cafe_participants->displayName;
		if (strpos($name, $displayName) !== false) {
			$bool = TRUE;
		}
	}
}

// Connect Vidyo Cafe to Cafe Room
if (!$bool) {
	try {
		$users = $userClient->search(array('Filter' => array('query' => $displayName)));
		$users = $users->Entity;
		if (is_array($users)) {		
		    foreach ($users as $user) {
			$name = $user->displayName;
			if (strpos($name, $displayName) !== false) {
				$entityID = $user->entityID;
				break;
			}

		    }
                } else {
                    $name = $users->displayName;
                    if (strpos($name, $displayName) !== false) {
                        $entityID = $users->entityID;
                    }
                }
                echo($entityID);
		$joinConference = $adminClient->inviteToConference(array('conferenceID' => $roomID, 'entityID' => $entityID));
		echo "Vidyo Cafe added to Cafe!\n";
	} catch (SoapFault $fault) {
		echo "Sorry, User API returned the following ERROR:\n".$fault->faultcode."-".$fault->faultstring."\n";
	}
}
